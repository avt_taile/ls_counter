﻿using System.Linq;
using System.Web.Mvc;
using Counter.Models;
using Counter.DAL;
using Counter.Services;
using Counter.Repository;

namespace Counter.Controllers
{
    public class HomeController : Controller
    {
        //public CounterContext _db = new CounterContext();
        private ICounterRepository _counterRepository;
        public CounterService _counterService;

        public HomeController()
        {
            this._counterRepository = new CounterRepository(new CounterContext());
            _counterService = new CounterService(_counterRepository);
        }

        public HomeController(ICounterRepository counterRepository)
        {
            this._counterRepository = counterRepository;
            _counterService = new CounterService(_counterRepository);
        }

        public ActionResult Index()
        {
            //get max number in db
            int maxNumber = _counterService.GetMaxNumber();
            ViewBag.Number = maxNumber;
            return View();
        }

        public ActionResult CounterClick()
        {
            _counterService.InsertMaxValue();
            ////get max number in db
            //int maxNumber = GetMaxNumber();

            ////validate 
            //if (IsAllowIncrease(maxNumber))
            //{
            //    //add new
            //    var myCounter = new MyCounter();
            //    myCounter.Number = maxNumber + 1;
            //    _db.Counters.Add(myCounter);

            //    //save
            //    _db.SaveChanges();
            //}
            return RedirectToAction("Index");
        }

        //private int GetMaxNumber()
        //{
        //    if (!_db.Counters.Any())
        //    {
        //        return 0;
        //    }

        //    return _db.Counters.Max(x => x.Number);
        //}

        //private bool IsAllowIncrease(int maxNumber)
        //{
        //    if (maxNumber >= 10)
        //        return false;
        //    return true;
        //}
    }
}