﻿using Counter.DAL;
using Counter.Models;
using System;
using System.Linq;

namespace Counter.Repository
{
    public class CounterRepository : ICounterRepository, IDisposable
    {
        private CounterContext _context;

        public CounterRepository(CounterContext context)
        {
            _context = context;
        }

        public void InsertMaxValue()
        {
            //get max number in db
            int maxNumber = GetMaxNumber();

            //validate 
            if (IsAllowIncrease(maxNumber))
            {
                //add new
                var myCounter = new MyCounter();
                myCounter.Number = maxNumber + 1;
                _context.Counters.Add(myCounter);

                //save
                _context.SaveChanges();
            }
        }

        public int GetMaxNumber()
        {
            if (!_context.Counters.Any())
            {
                return 0;
            }

            return _context.Counters.Max(x => x.Number);
        }

        private bool IsAllowIncrease(int maxNumber)
        {
            if (maxNumber >= 10)
                return false;
            return true;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}