﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Counter.Repository
{
    public interface ICounterRepository : IDisposable
    {
        void InsertMaxValue();
        int GetMaxNumber();
    }
}