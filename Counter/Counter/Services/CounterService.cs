﻿using Counter.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Counter.Services
{
    public class CounterService
    {
        private readonly ICounterRepository _counterRepository;
        public CounterService(ICounterRepository counterRepository)
        {
            _counterRepository = counterRepository;
        }

        public void InsertMaxValue()
        {
            try
            {
                _counterRepository.InsertMaxValue();
            }
            catch (Exception)
            {
                
            }
        }

        public int GetMaxNumber()
        {
           
            return _counterRepository.GetMaxNumber();
        }

    }
}