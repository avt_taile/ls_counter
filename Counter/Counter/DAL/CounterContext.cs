﻿using System.Data.Entity;
using Counter.Models;

namespace Counter.DAL
{
    public class CounterContext : DbContext
    {
        public CounterContext() : base("name=CounterConnectionString")
        {
        }

        public virtual DbSet<MyCounter> Counters { get; set; }
    }
}