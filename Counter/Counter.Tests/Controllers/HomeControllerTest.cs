﻿using NUnit.Framework;
using Counter.Repository;
using Counter.Services;
using Moq;
using System.Data.Entity;
using Counter.DAL;
using System.Collections.Generic;
using Counter.Models;
using System.Linq;

namespace Counter.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTest
    {
        public CounterContext _context;
        private int currentNumber = 5;

        [OneTimeSetUp]
        public void InitialData()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<CounterContext>());
            _context = new CounterContext();
        }
        [OneTimeTearDown]
        public void ClearDb()
        {
            Database.Delete("CounterConnectionString");
        }

        [Test]
        public void TestServiceIncreaseOK()
        {
            var icounterRepositoryMock = new Mock<ICounterRepository>();
            
            icounterRepositoryMock.Setup(x => x.GetMaxNumber()).Returns(() => currentNumber);
            icounterRepositoryMock.Setup(x => x.InsertMaxValue()).Callback(() => currentNumber = currentNumber + 1);

            CounterService counterService = new CounterService(icounterRepositoryMock.Object);
            counterService.InsertMaxValue();
            counterService.InsertMaxValue();
            int newvalue = counterService.GetMaxNumber();
            Assert.AreEqual(currentNumber, newvalue);
        }

        [Test]
        public void TestRepositoryIncreaseOK()
        {
            var context = new Mock<CounterContext>();
            var dbset = new Mock<DbSet<MyCounter>>();
            var list = new List<MyCounter>() { new MyCounter { Id = 0, Number = 5 } };

            var dbSetMock = new Mock<DbSet<MyCounter>>();
            dbSetMock.As<IQueryable<MyCounter>>().Setup(m => m.Provider)
                .Returns(list.AsQueryable().Provider);
            dbSetMock.As<IQueryable<MyCounter>>().Setup(m => m.Expression)
                    .Returns(list.AsQueryable().Expression);
            dbSetMock.As<IQueryable<MyCounter>>().Setup(m => m.ElementType)
                    .Returns(list.AsQueryable().ElementType);
            dbSetMock.As<IQueryable<MyCounter>>().Setup(m => m.GetEnumerator())
                    .Returns(list.GetEnumerator());

            dbSetMock.Setup(o => o.Add(It.IsAny<MyCounter>())).Callback((MyCounter a)=>list.Add(a));
            context.Setup(o => o.Counters).Returns(dbSetMock.Object);

            var repo = new CounterRepository(context.Object);
            var number = repo.GetMaxNumber();
            repo.InsertMaxValue();
            var newValue = repo.GetMaxNumber();
            Assert.AreEqual(6,newValue);
        }

        [Test]
        public void IntegrationTestIncreaseAllowOK()
        {
            var repo = new CounterRepository(_context);
            var service = new CounterService(repo);

            var maxNumber = service.GetMaxNumber();
            service.InsertMaxValue();
            var newValue = service.GetMaxNumber();
            Assert.AreEqual(maxNumber + 1, newValue);
        }

        [Test]
        public void IntegrationTestIncreaseMoreThan10IsNotAllowFailed()
        {
            var repo = new CounterRepository(_context);
            var service = new CounterService(repo);

            var maxNumber = service.GetMaxNumber();
            var counter = 15;

            //call more than 10 times
            for(int i = 0; i < counter; i++)
            {
                service.InsertMaxValue();
            }
            
            var newValue = service.GetMaxNumber();
            Assert.AreEqual(counter, newValue);
        }
    }
}
